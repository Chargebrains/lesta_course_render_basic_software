#pragma once
#include "default_types.hxx"

namespace soul_vessel_engine
{

class canvas final
{
public:
    explicit canvas(const size_t width, const size_t height);

    [[nodiscard]] auto to_image() const -> image;

    auto set_pixel(const position p, const color c) -> void;

    [[nodiscard]] auto get_pixel(const size_t x, const size_t y) const -> color;

    auto pixels() & -> pixel_colors& { return pixels_; }

    auto begin() { return pixels_.begin(); }
    auto end() { return pixels_.end(); }

    auto get_width() const -> size_t { return width_; }
    auto get_height() const -> size_t { return height_; }

private:
    [[nodiscard]] auto pos_to_linear_index(const size_t x, const size_t y) const -> size_t;

    pixel_colors pixels_;
    size_t       width_;
    size_t       height_;
};

} // namespace soul_vessel_engine
