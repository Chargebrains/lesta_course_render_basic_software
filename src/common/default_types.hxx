#pragma once

#include <cmath>
#include <cstddef>
#include <ostream>
#include <vector>

namespace soul_vessel_engine
{

///@brief coordinate value by axis
using coordinate = int;

///@brief A position in a 2D space.
struct position
{
    coordinate x{ 0u };
    coordinate y{ 0u };

    auto operator<=>(const position& rhs) const -> bool = default;
    auto operator-(const position& rhs) const -> position { return { x - rhs.x, y - rhs.y }; }
};

static auto distance(const position& lhs, const position& rhs) -> float
{
    return static_cast<float>(std::sqrt((lhs.x - rhs.x) ^ 2 + (lhs.y - rhs.y) ^ 2));
}

///@brief All pixels positions in a 2D space.
using pixels_pos = std::vector<position>;

///@brief Value for one color channel of pixel (r,g or b)
using color_channel = uint8_t;

static auto generate_random_color_chanel_value() -> color_channel
{
    return static_cast<color_channel>(rand() % 255);
}

static auto operator<<(std::ostream& os, const color_channel ch) -> std::ostream&
{
    return os << static_cast<size_t>(ch);
}

///@brief RGB color for one pixel
struct color
{
    color_channel r;
    color_channel g;
    color_channel b;
};
static auto generate_random_color() -> color
{
    return { generate_random_color_chanel_value(),
             generate_random_color_chanel_value(),
             generate_random_color_chanel_value() };
}

static auto operator<<(std::ostream& os, const color& color) -> std::ostream&
{
    return os << "r: " << color.r << " g: " << color.g << " b: " << color.b;
}

///@brief Predefined colors
namespace default_colors
{
static constexpr auto black{ color{ 0, 0, 0 } };
static constexpr auto white{ color{ 255, 255, 255 } };
static constexpr auto red{ color{ 255, 0, 0 } };
static constexpr auto green{ color{ 0, 255, 0 } };
static constexpr auto blue{ color{ 0, 0, 255 } };
} // namespace default_colors

using pixel_colors = std::vector<color>;

struct image
{
    pixel_colors pixels;
    size_t       width; // TODO do normal type for this
    size_t       height;
};

enum class primitive_type
{
    line,
    triangle,
    // circle // coming soon
    // point, // coming soon
};
struct line
{
    position begin{};
    position end{};
};

using lines = std::vector<line>;

struct triangle
{
    position p0;
    position p1;
    position p2;

    [[nodiscard]] auto to_lines() const -> lines { return { { p0, p1 }, { p1, p2 }, { p2, p0 } }; }
};

using triangles = std::vector<triangle>;

struct vertex
{
    float x;
    float y;

    float r;
    float g;
    float b;

    auto get_pos() const -> position { return { static_cast<coordinate>(x), static_cast<coordinate>(y) }; }
    auto get_color() const -> color
    {
        return { static_cast<color_channel>(r * 255),
                 static_cast<color_channel>(g * 255),
                 static_cast<color_channel>(b * 255) };
    }
};

static vertex create_interpolated_vertex(const vertex& v0, const vertex& v1, const float t)
{

    /*// v0 + t *( v1 - v0)
    auto r = std::sqrt(std::lerp(v0.r * v0.r, v1.r * v1.r, t));
    auto g = std::sqrt(std::lerp(v0.g * v0.g, v1.g * v1.g, t));
    auto b = std::sqrt(std::lerp(v0.b * v0.b, v1.b * v1.b, t));

    return vertex{ .x{ std::lerp(v0.x, v1.x, t) }, .y{ std::lerp(v0.y, v1.y, t) }, .r{ r }, .g{ g }, .b{ b } };*/

    return vertex{ .x{ std::lerp(v0.x, v1.x, t) },
                   .y{ std::lerp(v0.y, v1.y, t) },
                   .r{ std::lerp(v0.r, v1.r, t) },
                   .g{ std::lerp(v0.g, v1.g, t) },
                   .b{ std::lerp(v0.b, v1.b, t) } };
}

struct vertices
{
    std::vector<vertex> vertices;

    auto operator[](const size_t index) -> vertex& { return vertices[index]; }
    auto operator[](const size_t index) const -> const vertex& { return vertices[index]; }

    auto begin() { return vertices.begin(); }
    auto end() { return vertices.end(); }

    [[nodiscard]] auto to_lines() const -> lines
    {
        const auto vertices_count{ vertices.size() };
        if (vertices_count % 2 != 0)
        {
            throw std::runtime_error{ "Error: vertices count is not even. Can't create last line." };
        }

        lines lines;
        lines.resize(vertices_count / 2);
        for (size_t i{}; i < vertices_count; i += 2)
        {
            lines.emplace_back(vertices[i].get_pos(), vertices[i + 1].get_pos());
        }
        return lines;
    }

    [[nodiscard]] auto to_triangles() const -> triangles
    {
        const auto vertices_count{ vertices.size() };
        if (vertices_count % 3 != 0)
        {
            throw std::runtime_error{
                "Error: the number of vertices is not a multiple of three, we cannot make a triangle"
            };
        }

        triangles triangles;
        triangles.resize(vertices_count / 3);
        for (size_t i{}; i < vertices_count; i += 3)
        {
            triangles.emplace_back(vertices[i].get_pos(), vertices[i + 1].get_pos(), vertices[i + 2].get_pos());
        }
        return triangles;
    }
};

using index   = uint8_t;
using indices = std::vector<index>;

} // namespace soul_vessel_engine