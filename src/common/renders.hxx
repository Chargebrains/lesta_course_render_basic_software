#pragma once

#include "bresenham_algorithm.hxx"
#include "canvas.hxx"

#include <algorithm>

namespace soul_vessel_engine
{
class line_render
{
public:
    explicit line_render(canvas& canvas);

    auto draw_line(const line l, const color c) -> void;

private:
    canvas& canvas_;
};

class lines_render
{
public:
    explicit lines_render(canvas& canvas);

    auto draw_lines(const lines& lines, const color c) -> void;

private:
    canvas& canvas_;
};

class triangle_render
{
public:
    explicit triangle_render(canvas& canvas);

    auto draw_triangle(const triangle& triangle, const color c) -> void;

private:
    canvas& canvas_;
};

class triangles_render
{
public:
    explicit triangles_render(canvas& canvas);

    auto draw_triangles(const triangles& triangles, const color c) -> void;

private:
    canvas& canvas_;
};

class vertices_render
{
public:
    explicit vertices_render(canvas& canvas);

    auto draw_vertices(const vertices& vertices, const primitive_type primitive, const color c) -> void;

private:
    canvas& canvas_;
};

class indexed_render
{
public:
    explicit indexed_render(canvas& canvas);

    auto draw_indexed(const vertices& vertices, const indices& indices, const primitive_type primitive, const color c)
        -> void;

private:
    canvas& canvas_;
};

class triangle_interpolated_render
{
public:
    explicit triangle_interpolated_render(canvas& canvas);

    auto draw_triangle_interpolated(const vertex& v0, const vertex& v1, const vertex& v2) -> void;

private:
    canvas& canvas_;
};

class triangles_interpolated_render
{
public:
    explicit triangles_interpolated_render(canvas& canvas);

    auto draw_triangles_interpolated(const vertices& vertices, const indices& indices) -> void;

private:
    canvas& canvas_;
};

} // namespace soul_vessel_engine