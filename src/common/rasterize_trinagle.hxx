#pragma once

#include "default_types.hxx"

namespace soul_vessel_engine
{
auto rasterize_triangle(const vertex& v0, const vertex& v1, const vertex& v2) -> vertices;
} // namespace soul_vessel_engine