#pragma once

#include "default_types.hxx"

#include <string>

namespace soul_vessel_engine::ppm_io
{

auto load_image(const std::string& filename) -> image;
auto save_image(const std::string& filename, const image& img) -> void;

} // namespace soul_vessel_engine::ppm_io