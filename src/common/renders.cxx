#include "renders.hxx"
#include "rasterize_trinagle.hxx"

#include <cmath>
#include <ranges>

namespace soul_vessel_engine
{
line_render::line_render(canvas& canvas)
    : canvas_{ canvas }
{
}

auto line_render::draw_line(const line l, const color c) -> void
{
    const auto line_pixels_pos{ bresenham::pixels_positions(l.begin.x, l.begin.y, l.end.x, l.end.y) };
    std::ranges::for_each(line_pixels_pos, [&](const auto& pos) { canvas_.set_pixel(pos, c); });
}

lines_render::lines_render(canvas& canvas)
    : canvas_{ canvas }
{
}

auto lines_render::draw_lines(const lines& lines, const color c) -> void
{
    auto lr{ line_render{ canvas_ } };
    std::ranges::for_each(lines, [&](const auto& l) { lr.draw_line(l, c); });
}

triangle_render::triangle_render(canvas& canvas)
    : canvas_{ canvas }
{
}

auto triangle_render::draw_triangle(const triangle& triangle, const color c) -> void
{
    auto lines_renderer{ lines_render{ canvas_ } };
    lines_renderer.draw_lines(triangle.to_lines(), c);
}

triangles_render::triangles_render(canvas& canvas)
    : canvas_{ canvas }
{
}

auto triangles_render::draw_triangles(const triangles& triangles, const color c) -> void
{
    auto triangle_renderer{ triangle_render{ canvas_ } };
    std::ranges::for_each(triangles, [&](const auto& t) { triangle_renderer.draw_triangle(t, c); });
}

vertices_render::vertices_render(canvas& canvas)
    : canvas_{ canvas }
{
}

auto vertices_render::draw_vertices(const vertices& vertices, const primitive_type primitive, const color c) -> void
{
    switch (primitive)
    {
        case primitive_type::line:
        {
            lines_render lr{ canvas_ };
            lr.draw_lines(vertices.to_lines(), c);
            break;
        }
        case primitive_type::triangle:
        {
            triangles_render tr{ canvas_ };
            tr.draw_triangles(vertices.to_triangles(), c);
            break;
        }
        default:
            throw std::runtime_error{ "error: unknown primitive type." };
    }
}

indexed_render::indexed_render(canvas& canvas)
    : canvas_{ canvas }
{
}

auto indexed_render::draw_indexed(const vertices&      vertices,
                                  const indices&       indices,
                                  const primitive_type primitive,
                                  const color          c) -> void
{
    switch (primitive)
    {
        case primitive_type::line:
        {
            if (indices.size() % 2 != 0)
            {
                throw std::runtime_error{ "error: number of indices must be an even number. Can't create last line" };
            }

            line_render line_render{ canvas_ };
            for (size_t index{}; index < indices.size(); index += 2)
            {
                line_render.draw_line(line{ .begin{ vertices[indices[index + 0]].get_pos() },
                                            .end{ vertices[indices[index + 1]].get_pos() } },
                                      c);
            }
            break;
        }
        case primitive_type::triangle:
        {
            if (indices.size() % 3 != 0)
            {
                throw std::runtime_error{
                    "Error: the number of vertices is not a multiple of three, we cannot make a triangle. "
                };
            }

            triangle_render triangle_render{ canvas_ };
            for (size_t index{}; index < indices.size(); index += 3)
            {
                const triangle&& triangle{ .p0{ vertices[indices[index + 0]].get_pos() },
                                           .p1{ vertices[indices[index + 1]].get_pos() },
                                           .p2{ vertices[indices[index + 2]].get_pos() } };

                triangle_render.draw_triangle(triangle, c);
            }
            break;
        }
        default:
            throw std::runtime_error{ "error: unknown primitive type." };
    }
}

triangle_interpolated_render::triangle_interpolated_render(canvas& canvas)
    : canvas_{ canvas }
{
}
auto triangle_interpolated_render::draw_triangle_interpolated(const vertex& v0, const vertex& v1, const vertex& v2)
    -> void
{

    const vertices&& rasterized_vertices{ soul_vessel_engine::rasterize_triangle(v0, v1, v2) };
    for (const auto& vertex : rasterized_vertices.vertices)
    {
        const color    c{ vertex.get_color() };
        const position pos{ vertex.get_pos() };
        canvas_.set_pixel(pos, c);
    }
}

triangles_interpolated_render::triangles_interpolated_render(canvas& canvas)
    : canvas_{ canvas }
{
}
auto triangles_interpolated_render::draw_triangles_interpolated(const vertices& vertices, const indices& indices)
    -> void
{
    if (indices.size() % 3 != 0)
    {
        throw std::runtime_error{
            "Error: the number of vertices is not a multiple of three, we cannot make a triangle. "
        };
    }

    triangle_interpolated_render triangle_render{ canvas_ };
    for (size_t index{}; index < indices.size(); index += 3)
    {
        const vertex v0{ vertices[indices[index + 0]] };
        const vertex v1{ vertices[indices[index + 1]] };
        const vertex v2{ vertices[indices[index + 2]] };

        triangle_render.draw_triangle_interpolated(v0, v1, v2);
    }
}

} // namespace soul_vessel_engine