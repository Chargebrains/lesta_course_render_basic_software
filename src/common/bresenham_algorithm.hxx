#pragma once

#include <functional>

#include "default_types.hxx"

namespace bresenham
{
static auto pixels_positions(const int x0_input, const int y0_input, const int x1_input, const int y1_input)
    -> soul_vessel_engine::pixels_pos
{
    // THis algorithm with a few dirty hacks to reduce the number of lines of code. sorry.

    namespace sve = soul_vessel_engine;

    int x0{ x0_input };
    int y0{ y0_input };
    int x1{ x1_input };
    int y1{ y1_input };

    bool steep{ false };

    if (std::abs(x0 - x1) < std::abs(y0 - y1))
    {
        std::swap(x0, y0);
        std::swap(x1, y1);
        steep = true;
    }

    if (x0 > x1)
    {
        std::swap(x0, x1);
        std::swap(y0, y1);
    }

    const int dx{ x1 - x0 };
    const int dx2{ dx * 2 };
    const int dy{ y1 - y0 };
    const int derror2{ std::abs(dy) * 2 };
    int       y{ y0 };
    int       iteration_step{ y1 > y0 ? 1 : -1 };
    int       error2{ 0 };

    sve::pixels_pos result;

    std::function emplace_back_to_result{ [&](const int x, const int y) { result.emplace_back(x, y); } };
    if (steep)
    {
        emplace_back_to_result = [&](const int x, const int y) { result.emplace_back(y, x); };
    }

    for (int x{ x0 }; x <= x1; ++x)
    {
        emplace_back_to_result(x, y);

        error2 += derror2;
        if (error2 > dx)
        {
            y += iteration_step;
            error2 -= dx2;
        }
    }

    return result;
}
static auto pixels_positions(const soul_vessel_engine::position p1, const soul_vessel_engine::position p2)
    -> soul_vessel_engine::pixels_pos
{
    return pixels_positions(p1.x, p1.y, p2.x, p2.y);
}

} // namespace bresenham