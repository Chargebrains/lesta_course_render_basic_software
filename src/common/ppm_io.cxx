#include "ppm_io.hxx"

#include <fstream>

// TODO refactor this shitty

namespace soul_vessel_engine::ppm_io
{
auto load_image(const std::string& filename) -> image
{
    image result{};

    std::ifstream in_file;
    in_file.exceptions(std::ios_base::failbit);
    in_file.open(filename, std::ios_base::binary);
    std::string header;
    std::string color_format;
    char        last_next_line = 0;

    in_file >> header >> result.width >> result.height >> color_format >> std::noskipws >> last_next_line;

    if (!iswspace(last_next_line))
    {
        throw std::runtime_error("expected whitespace");
    }

    result.pixels.resize(result.width * result.height);

    if (result.pixels.size() != result.width * result.height)
    {
        throw std::runtime_error("image size not match");
    }
    auto buf_size = static_cast<std::streamsize>(sizeof(color) * result.pixels.size());
    in_file.read(reinterpret_cast<char*>(result.pixels.data()), buf_size);
    return result;
}

auto save_image(const std::string& filename, const image& img) -> void
{
    std::ofstream out_file;
    out_file.exceptions(std::ios_base::failbit);
    out_file.open(filename, std::ios_base::binary);
    // clang-format off
        out_file << "P6\n"
                 << img.width << ' ' << img.height << ' ' << 255 << '\n';
    // clang-format on
    auto buf_size = static_cast<std::streamsize>(sizeof(color) * img.pixels.size());
    out_file.write(reinterpret_cast<const char*>(img.pixels.data()), buf_size);
}
} // namespace soul_vessel_engine::ppm_io