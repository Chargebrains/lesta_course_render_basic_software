#pragma once

#include "default_types.hxx"

#include <cstddef>

namespace soul_vessel_engine::config::canvas
{
static constinit size_t width{ 600 };
static constinit size_t height{ 600 };
static constinit color  default_background{ soul_vessel_engine::default_colors::black };
} // namespace soul_vessel_engine::config::canvas