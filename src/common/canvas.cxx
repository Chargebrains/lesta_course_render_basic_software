#include "canvas.hxx"

namespace soul_vessel_engine
{
canvas::canvas(const size_t width, const size_t height)
    : pixels_{}
    , width_{ width }
    , height_{ height }
{
    pixels_.resize(width_ * height_);
}

auto canvas::to_image() const -> image
{
    return image{ .pixels{ pixels_ }, .width{ width_ }, .height{ height_ } };
}

auto canvas::set_pixel(const position p, const color c) -> void
{
    if (p.x >= width_ || p.y >= height_)
    {
        return;
    }

    pixels_.at(pos_to_linear_index(p.x, p.y)) = c;
}

auto canvas::get_pixel(const size_t x, const size_t y) const -> color
{
    return pixels_.at(pos_to_linear_index(x, y));
}

auto canvas::pos_to_linear_index(const size_t x, const size_t y) const -> size_t
{
    return width_ * y + x;
}

} // namespace soul_vessel_engine