#include "bresenham_algorithm.hxx"
#include "rasterize_trinagle.hxx"

#include <algorithm>
#include <ranges>

namespace soul_vessel_engine
{
auto rasterize_one_horizontal_line(const vertex& lv, const vertex& rv) -> vertices
{
    vertices out;

    size_t num_of_pixels_in_line{ static_cast<size_t>(std::round(std::abs(lv.x - rv.x))) };
    if (num_of_pixels_in_line > 0)
    {
        for (size_t p{}; p <= num_of_pixels_in_line + 1; ++p)
        {
            const auto     t_pixel{ static_cast<float>(p) / static_cast<float>(num_of_pixels_in_line + 1) };
            const vertex&& pixel{ create_interpolated_vertex(lv, rv, t_pixel) };
            out.vertices.emplace_back(pixel);
        }
    }
    else
    {
        out.vertices.push_back(lv);
    }

    return out;
}

auto rasterize_horizontal_triangle(const vertex& single, const vertex& left, const vertex& right) -> vertices
{
    vertices out;

    if (const auto hor_lines_count{ static_cast<size_t>(std::round(std::abs(single.y - left.y))) }; hor_lines_count > 0)
    {
        for (size_t i{}; i <= hor_lines_count; ++i)
        {
            const float  t_vertical{ static_cast<float>(i) / static_cast<float>(hor_lines_count + 1) };
            const vertex left_vertex{ create_interpolated_vertex(left, single, t_vertical) };
            const vertex right_vertex{ create_interpolated_vertex(right, single, t_vertical) };

            const auto line_vertices{ rasterize_one_horizontal_line(left_vertex, right_vertex) };
            out.vertices.insert(out.vertices.end(), line_vertices.vertices.begin(), line_vertices.vertices.end());
        }
    }
    else
    {
        const auto line_vertices{ rasterize_one_horizontal_line(left, right) };
        out.vertices.insert(out.vertices.end(), line_vertices.vertices.begin(), line_vertices.vertices.end());
    }

    return out;
}

auto rasterize_triangle(const vertex& v0, const vertex& v1, const vertex& v2) -> vertices
{
    namespace sve = soul_vessel_engine;
    vertices out;

    std::array in_vertices{ v0, v1, v2 };
    std::ranges::sort(in_vertices, [](const vertex& left, const vertex& right) { return left.y < right.y; });
    const auto& [top, mid, bottom]{ in_vertices };

    const position start{ top.get_pos() };
    const position end{ bottom.get_pos() };

    pixels_pos longest_side_line{ bresenham::pixels_positions(start, end) };

    const auto it_middle{ std::ranges::find_if(
        longest_side_line, [&](const position& pos) { return pos.y == static_cast<int32_t>(std::round(mid.y)); }) };

    position second_middle{ *it_middle };

    // create_interpolated_vertex second_middle position to get 4 vertex
    float       t{ 0 };
    const float end_start{ sve::distance(end, start) };
    if (end_start > 0)
    {
        const float middle_start = sve::distance(second_middle, start);
        t                        = middle_start / end_start;
    }

    vertex second_middle_vertex{ sve::create_interpolated_vertex(top, bottom, t) };

    const vertices&& top_triangle{ rasterize_horizontal_triangle(top, mid, second_middle_vertex) };
    const vertices&& bottom_triangle{ rasterize_horizontal_triangle(bottom, mid, second_middle_vertex) };

    out.vertices.insert(out.vertices.end(), top_triangle.vertices.begin(), top_triangle.vertices.end());
    out.vertices.insert(out.vertices.end(), bottom_triangle.vertices.begin(), bottom_triangle.vertices.end());

    return out;
}

} // namespace soul_vessel_engine