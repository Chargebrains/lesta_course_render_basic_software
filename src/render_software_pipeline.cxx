#include <SDL3/SDL.h>

#include <cstdlib>
#include <iostream>
#include <memory>
#include <random>

#include "common/canvas.hxx"
#include "common/default_types.hxx"
#include "common/rasterize_trinagle.hxx"

struct uniform
{
    float mouse_x;
    float mouse_y;

    float radius;

    float time; // ms
};

class i_vertex_shader
{
public:
    virtual auto operator()(const soul_vessel_engine::vertex& vertex, const uniform& uniform)
        -> soul_vessel_engine::vertex const = 0;

    virtual ~i_vertex_shader() = default;
};

class default_vertex_shader final : public i_vertex_shader
{
public:
    auto operator()(const soul_vessel_engine::vertex& vertex, const uniform& uniform)
        -> soul_vessel_engine::vertex const override
    {
        return vertex;
    }

    ~default_vertex_shader() override = default;
};

class random_scaled_vertex_shader final : public i_vertex_shader
{
public:
    auto operator()(const soul_vessel_engine::vertex& vertex, const uniform& uniform)
        -> soul_vessel_engine::vertex const override
    {
        auto result_vertex{ vertex };
        result_vertex.x += rand() % 10;
        result_vertex.y += rand() % 10;

        return result_vertex;
    }

    ~random_scaled_vertex_shader() override = default;
};

class vertex_shader final : public i_vertex_shader
{
public:
    auto operator()(const soul_vessel_engine::vertex& vertex, const uniform& uniform)
        -> soul_vessel_engine::vertex const override
    {
        auto out{ vertex };

        out.x *= 0.5;
        out.y *= 0.5;
        out.x += uniform.radius;
        out.y += uniform.radius;

        return out;
    }

    ~vertex_shader() override = default;
};

class i_fragment_shader
{
public:
    virtual auto operator()(const soul_vessel_engine::vertex& vertex, const uniform& uniform)
        -> soul_vessel_engine::color const = 0;

    virtual ~i_fragment_shader() = default;
};

class default_fragment_shader final : public i_fragment_shader
{
public:
    auto operator()(const soul_vessel_engine::vertex& vertex, const uniform& uniform)
        -> soul_vessel_engine::color const override
    {
        return vertex.get_color();
    }

    ~default_fragment_shader() override = default;
};

class inverted_color_fragment_shader final : public i_fragment_shader
{
public:
    auto operator()(const soul_vessel_engine::vertex& vertex, const uniform& uniform)
        -> soul_vessel_engine::color const override
    {
        soul_vessel_engine::color result_color{ vertex.get_color() };
        result_color.r = 255 - result_color.r;
        result_color.g = 255 - result_color.g;
        result_color.b = 255 - result_color.b;
        return result_color;
    }

    ~inverted_color_fragment_shader() override = default;
};

class digital_noise_fragment_shader final : public i_fragment_shader
{
public:
    auto operator()(const soul_vessel_engine::vertex& vertex, const uniform& uniform)
        -> soul_vessel_engine::color const override
    {
        soul_vessel_engine::color result_color{ vertex.get_color() };

        if (const bool is_noise_pixel{ static_cast<bool>(rand() & 2) }; !is_noise_pixel)
        {
            return result_color;
        }

        const uint8_t noise_channel{ static_cast<uint8_t>(rand() % 3) };
        switch (noise_channel)
        {
            case 0:
                result_color.r = rand() % 255;
                break;
            case 1:
                result_color.g = rand() % 255;
                break;
            case 2:
                result_color.b = rand() % 255;
                break;
        }

        return result_color;
    }

    ~digital_noise_fragment_shader() override = default;
};

class mouse_strange_waves_fragment_shader final : public i_fragment_shader
{
public:
    auto operator()(const soul_vessel_engine::vertex& vertex, const uniform& uniform)
        -> soul_vessel_engine::color const override
    {
        const float t{ uniform.time };

        constexpr auto a{ 7 };

        const float  dx{ vertex.x - uniform.mouse_x };
        const float  dy{ vertex.y - uniform.mouse_y };
        const size_t distance_to_mouse{ static_cast<size_t>(std::sqrt(dx * dx + dy * dy)) };

        const auto current_radius{ std::abs(uniform.radius + a * cos(0.02 * t)) };

        soul_vessel_engine::color result_color{ vertex.get_color() };

        if (distance_to_mouse % static_cast<size_t>(current_radius + 1) <= 5 &&
            distance_to_mouse <= 25 * uniform.radius)
        {
            result_color.r = 255 - rand() % 255;
            result_color.g = 255 - rand() % 255;
            result_color.b = 255 - rand() % 255;
        }

        return result_color;
    }

    ~mouse_strange_waves_fragment_shader() override = default;
};

class render_pipeline
{
public:
    render_pipeline()
        : vertex_shader_{ std::make_unique<default_vertex_shader>() }
        , fragment_shader_{ std::make_unique<default_fragment_shader>() }
        , canvas_{ 600u, 600u }
    {
    }

    auto render(const soul_vessel_engine::vertices& vertices, const soul_vessel_engine::indices& indices) -> void
    {
        namespace sve = soul_vessel_engine;

        if (vertices.vertices.size() % 3 != 0)
        {
            throw std::runtime_error{ "Error: invalid number of vertices. Impossible create last triangle " };
        }

        if (vertex_shader_ == nullptr)
        {
            throw std::runtime_error{ "Error vertex shader is nullptr" };
        }

        if (fragment_shader_ == nullptr)
        {
            throw std::runtime_error{ "Error fragment shader is nullptr" };
        }

        for (size_t i{}; i < indices.size(); i += 3)
        {
            const sve::index index0{ indices.at(i + 0) };
            const sve::index index1{ indices.at(i + 1) };
            const sve::index index2{ indices.at(i + 2) };

            const sve::vertex& v0{ vertices.vertices.at(index0) };
            const sve::vertex& v1{ vertices.vertices.at(index1) };
            const sve::vertex& v2{ vertices.vertices.at(index2) };

            const sve::vertex v0_{ (*vertex_shader_)(v0, uniform_) };
            const sve::vertex v1_{ (*vertex_shader_)(v1, uniform_) };
            const sve::vertex v2_{ (*vertex_shader_)(v2, uniform_) };

            const auto&& rasterized_triangle{ sve::rasterize_triangle(v0_, v1_, v2_) };
            for (const auto& vertex : rasterized_triangle.vertices)
            {
                const auto pixel_color{ (*fragment_shader_)(vertex, uniform_) };
                const auto pixel_position{ vertex.get_pos() };
                canvas_.set_pixel(pixel_position, pixel_color);
            }
        }
    }

    auto set_uniform(const uniform& uniform) -> void { uniform_ = uniform; }
    auto set_vertex_shader(std::unique_ptr<i_vertex_shader> shader) -> void { vertex_shader_ = std::move(shader); }
    auto set_fragment_shader(std::unique_ptr<i_fragment_shader> shader) -> void
    {
        fragment_shader_ = std::move(shader);
    }

    auto get_image() const -> soul_vessel_engine::image { return canvas_.to_image(); }
    auto clear() -> void { std::ranges::fill(canvas_.pixels(), soul_vessel_engine::default_colors::black); }

private:
    uniform                            uniform_;
    std::unique_ptr<i_vertex_shader>   vertex_shader_;
    std::unique_ptr<i_fragment_shader> fragment_shader_;
    soul_vessel_engine::canvas         canvas_;
};

int main(int, char**)
{
    namespace sve = soul_vessel_engine;

    if (0 != SDL_Init(SDL_INIT_EVERYTHING))
    {
        std::cerr << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    SDL_Window* window = SDL_CreateWindow("runtime soft render", 601u, 601u, SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        std::cerr << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, nullptr, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr)
    {
        std::cerr << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    render_pipeline render_pipeline;

    render_pipeline.set_vertex_shader(std::make_unique<default_vertex_shader>());
    //    render_pipeline.set_vertex_shader(std::make_unique<random_scaled_vertex_shader>());
    render_pipeline.set_vertex_shader(std::make_unique<vertex_shader>());

    render_pipeline.set_fragment_shader(std::make_unique<default_fragment_shader>());
    render_pipeline.set_fragment_shader(std::make_unique<inverted_color_fragment_shader>());
    render_pipeline.set_fragment_shader(std::make_unique<digital_noise_fragment_shader>());
    render_pipeline.set_fragment_shader(std::make_unique<mouse_strange_waves_fragment_shader>());

    const sve::vertices&& vertices{ .vertices{ { .x = 350.f, .y = 350.f, .r = 1.f, .g = 1.f, .b = 1.f },
                                               { .x = 10.f, .y = 10.f, .r = 1.f, .g = 0.f, .b = 0.f },
                                               { .x = 350.f, .y = 10.f, .r = 1.f, .g = 0.5f, .b = 1.f },
                                               { .x = 590.f, .y = 10.f, .r = 1.f, .g = 0.6f, .b = 0.f },
                                               { .x = 590.f, .y = 350.f, .r = 1.f, .g = 1.f, .b = 0.f },
                                               { .x = 590.f, .y = 590.f, .r = 0.f, .g = 1.f, .b = 0.f },
                                               { .x = 350.f, .y = 590.f, .r = 0.2f, .g = 0.6f, .b = .9f },
                                               { .x = 10.f, .y = 590.f, .r = 0.f, .g = 0.f, .b = 1.f },
                                               { .x = 10.f, .y = 350.f, .r = 0.6f, .g = 0.f, .b = 1.f } } };

    const sve::indices indices{ 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 6, 0, 7, 6, 0, 7, 8, 0, 8, 1 };

    float mouse_x{};
    float mouse_y{};
    float radius{ 10.0 }; // 20 pixels radius
    float time{};         // ms

    bool continue_loop = true;

    while (continue_loop)
    {
        const size_t start_time{ SDL_GetTicks() };
        SDL_Event    e;
        while (SDL_PollEvent(&e))
        {
            if (e.type == SDL_EVENT_QUIT)
            {
                continue_loop = false;
                break;
            }
            else if (e.type == SDL_EVENT_MOUSE_MOTION)
            {
                mouse_x = e.motion.x;
                mouse_y = e.motion.y;
            }
            else if (e.type == SDL_EVENT_MOUSE_WHEEL)
            {
                radius += e.wheel.y;
            }
        }
        render_pipeline.clear();
        render_pipeline.set_uniform({ .mouse_x = mouse_x, .mouse_y = mouse_y, .radius = radius, .time = time });
        render_pipeline.render(vertices, indices);

        auto  image{ render_pipeline.get_image() };
        void* pixels(image.pixels.data());

        SDL_Surface* bitmapSurface = SDL_CreateSurfaceFrom(
            pixels, image.width, image.height, image.width * sizeof(sve::color), SDL_PIXELFORMAT_RGB24);
        if (bitmapSurface == nullptr)
        {
            std::cerr << SDL_GetError() << std::endl;
            return EXIT_FAILURE;
        }
        SDL_Texture* bitmapTex = SDL_CreateTextureFromSurface(renderer, bitmapSurface);
        if (bitmapTex == nullptr)
        {
            std::cerr << SDL_GetError() << std::endl;
            return EXIT_FAILURE;
        }
        SDL_DestroySurface(bitmapSurface);

        SDL_RenderClear(renderer);
        SDL_RenderTexture(renderer, bitmapTex, nullptr, nullptr);
        SDL_RenderPresent(renderer);

        SDL_DestroyTexture(bitmapTex);

        time += (static_cast<float>(SDL_GetTicks() - start_time));
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return EXIT_SUCCESS;
}