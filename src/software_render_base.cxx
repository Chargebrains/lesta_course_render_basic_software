#include "common/canvas.hxx"
#include "common/config_defines.hxx"
#include "common/default_types.hxx"
#include "common/ppm_io.hxx"
#include "common/renders.hxx"

#include <algorithm>
#include <array>
#include <iostream>

auto do_software_render_homework() -> void;

int main(int, char**)
{
    try
    {
        do_software_render_homework();
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    catch (...)
    {
        std::cerr << "OOOPS..." << std::endl;
    }
}

auto do_ppm_example() -> void;
auto do_canvas_example() -> void;
auto do_line_render() -> void;
auto do_lines_render() -> void;
auto do_triangle_render() -> void;
auto do_triangles_render() -> void;
auto do_vertices_render() -> void;
auto do_indexes_render() -> void;
auto do_interpolated_triangle_render() -> void;
auto do_interpolated_triangles_render() -> void;
auto do_triangle_fun() -> void;

auto do_software_render_homework() -> void
{
    do_ppm_example();
    do_canvas_example();
    do_line_render();
    do_lines_render();
    do_triangle_render();
    do_triangles_render();
    do_vertices_render();
    do_indexes_render();
    do_interpolated_triangle_render();
    do_interpolated_triangles_render();
    do_triangle_fun();
}

auto do_ppm_example() -> void
{
    // Example of loading and saving changed ppm's image
    namespace sve = soul_vessel_engine;

    auto leo_image{ sve::ppm_io::load_image("./leo.ppm") };
    std::ranges::reverse(leo_image.pixels);
    sve::ppm_io::save_image("./results/reversed_leo.ppm", leo_image);
}

auto do_canvas_example() -> void
{
    // Example of creating canvas with solid color and with random colors
    namespace sve = soul_vessel_engine;

    // Example with solid red color
    {
        sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };
        std::ranges::fill(canvas, sve::default_colors::red);
        sve::ppm_io::save_image("./results/canvas_solid_color.ppm", canvas.to_image());
    }

    // Example with random pixel color
    {
        sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };
        std::ranges::generate(canvas, sve::generate_random_color);
        sve::ppm_io::save_image("./results/canvas_random_colors.ppm", canvas.to_image());
    }
}

auto do_line_render() -> void
{
    namespace sve = soul_vessel_engine;

    // Example of creating two lines from angle to angle

    sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

    // clang-format off
    // boilerplate a little bit
    const auto first_line{ sve::line{ .begin{ 0, 0 },
                                      .end{ canvas.get_width() - 1, canvas.get_height() - 1 } } };

    const auto second_line{ sve::line{ .begin{ canvas.get_width() - 1, 0 },
                                       .end{ 0, canvas.get_height() - 1 } } };

    const auto third_line{ sve::line{ .begin{ 0, canvas.get_height() / 2 },
                                      .end{ canvas.get_width() - 1, canvas.get_height() / 2 } } };

    const auto forth_line{ sve::line{ .begin{ canvas.get_width() / 2, 0 },
                                      .end{ canvas.get_width() / 2, canvas.get_height() - 1 } } };

    sve::line_render line_render{ canvas };
    std::ranges::for_each(std::array{ first_line, second_line, third_line, forth_line },
                          [&](const auto& line) { line_render.draw_line(line, sve::default_colors::green); });

    // clang-format on
    sve::ppm_io::save_image("./results/line_render.ppm", canvas.to_image());
}

auto do_lines_render() -> void
{
    namespace sve = soul_vessel_engine;

    // Example of creating lines with lines_renderer

    sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

    const auto&& lines{ sve::lines{
        sve::line{ .begin{ 0, 0 }, .end{ canvas.get_width() - 1, canvas.get_height() - 1 } },
        sve::line{ .begin{ canvas.get_width() - 1, 0 }, .end{ 0, canvas.get_height() - 1 } },
        sve::line{ .begin{ 0, canvas.get_height() / 2 }, .end{ canvas.get_width() - 1, canvas.get_height() / 2 } },
        sve::line{ .begin{ canvas.get_width() / 2, 0 }, .end{ canvas.get_width() / 2, canvas.get_height() - 1 } } } };

    sve::lines_render{ canvas }.draw_lines(lines, sve::default_colors::white);

    sve::ppm_io::save_image("./results/lines_render.ppm", canvas.to_image());
}

auto do_triangle_render() -> void
{
    namespace sve = soul_vessel_engine;

    // Example of creating lines with lines_renderer

    sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

    const auto width{ canvas.get_width() };
    const auto height{ canvas.get_height() };

    const sve::triangle triangle{ .p0{ .x = 0, .y = height - 1 },
                                  .p1{ .x = width / 2, .y = 0 },
                                  .p2{ .x = width - 1, .y = height - 1 } };

    sve::triangle_render{ canvas }.draw_triangle(triangle, sve::default_colors::red);
    sve::ppm_io::save_image("./results/triangle_render.ppm", canvas.to_image());
}

auto do_triangles_render() -> void
{
    namespace sve = soul_vessel_engine;

    // Example of creating lines with lines_renderer

    sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

    const auto width{ static_cast<int>(canvas.get_width()) };
    const auto height{ static_cast<int>(canvas.get_height()) };

    const sve::triangle&& t0{ .p0{ .x = 0, .y = 0 }, .p1{ .x = width / 2, .y = 0 }, .p2{ .x = 0, .y = height - 1 } };

    const sve::triangle&& t1{ .p0{ .x = 0, .y = height - 1 },
                              .p1{ .x = width / 2, .y = 0 },
                              .p2{ .x = width - 1, .y = height - 1 } };

    const sve::triangle&& t2{ .p0{ .x = width - 1, .y = height - 1 },
                              .p1{ .x = width / 2, .y = 0 },
                              .p2{ .x = width - 1, .y = 0 } };

    sve::triangles_render{ canvas }.draw_triangles({ t0, t1, t2 }, sve::default_colors::red);
    sve::ppm_io::save_image("./results/triangles_render.ppm", canvas.to_image());
}

auto do_vertices_render() -> void
{
    namespace sve = soul_vessel_engine;
    // Example of render vertices like triangles
    {
        sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

        const auto width{ static_cast<float>(canvas.get_width()) };
        const auto height{ static_cast<float>(canvas.get_height()) };

        const sve::vertices&& vertices{ .vertices{ { .x = 0, .y = 0 },
                                                   { .x = width / 2, .y = 0 },
                                                   { .x = 0, .y = height - 1 },
                                                   { .x = 0, .y = height - 1 },
                                                   { .x = width / 2, .y = 0 },
                                                   { .x = width - 1, .y = height - 1 },
                                                   { .x = width - 1, .y = height - 1 },
                                                   { .x = width / 2, .y = 0 },
                                                   { .x = width - 1, .y = 0 } } };

        sve::vertices_render{ canvas }.draw_vertices(
            vertices, sve::primitive_type::triangle, sve::default_colors::blue);
        sve::ppm_io::save_image("./results/vertices_render_triangles_types.ppm", canvas.to_image());
    }

    // Example of render vertices like lines
    {
        sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

        const auto width{ static_cast<float>(canvas.get_width()) };
        const auto height{ static_cast<float>(canvas.get_height()) };

        const sve::vertices&& vertices{ .vertices{ { .x = 0, .y = 0 },
                                                   { .x = width - 1, .y = height - 1 },
                                                   { .x = width - 1, .y = 0 },
                                                   { .x = 0, .y = height - 1 } } };

        sve::vertices_render{ canvas }.draw_vertices(vertices, sve::primitive_type::line, sve::default_colors::blue);
        sve::ppm_io::save_image("./results/vertices_render_lines_types.ppm", canvas.to_image());
    }
}

auto do_indexes_render() -> void
{
    namespace sve = soul_vessel_engine;
    // Example of render vertices like triangles
    {
        sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

        const auto width{ static_cast<float>(canvas.get_width()) };
        const auto height{ static_cast<float>(canvas.get_height()) };

        const sve::vertices&& vertices{ .vertices{ { .x = 0, .y = 0 },
                                                   { .x = width / 2, .y = 0 },
                                                   { .x = 0, .y = height - 1 },
                                                   { .x = 0, .y = height - 1 },
                                                   { .x = width / 2, .y = 0 },
                                                   { .x = width - 1, .y = height - 1 },
                                                   { .x = width - 1, .y = height - 1 },
                                                   { .x = width / 2, .y = 0 },
                                                   { .x = width - 1, .y = 0 } } };

        const sve::indices&& indices{ 0, 1, 5, 7, 6, 8 };

        sve::indexed_render{ canvas }.draw_indexed(
            vertices, indices, sve::primitive_type::triangle, sve::default_colors::blue);
        sve::ppm_io::save_image("./results/indexed_render_triangles_types.ppm", canvas.to_image());
    }

    // Example of render vertices like lines
    {
        sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

        const auto width{ static_cast<float>(canvas.get_width()) };
        const auto height{ static_cast<float>(canvas.get_height()) };

        const sve::vertices&& vertices{ .vertices{ { .x = 0, .y = 0 },
                                                   { .x = width - 1, .y = height - 1 },
                                                   { .x = width - 1, .y = 0 },
                                                   { .x = 0, .y = height - 1 } } };

        const sve::indices&& indices{ 3, 2 };

        sve::indexed_render{ canvas }.draw_indexed(
            vertices, indices, sve::primitive_type::line, sve::default_colors::blue);
        sve::ppm_io::save_image("./results/indexed_render_lines_types.ppm", canvas.to_image());
    }
}

auto do_interpolated_triangle_render() -> void
{
    namespace sve = soul_vessel_engine;

    sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

    const auto width{ static_cast<float>(canvas.get_width()) };
    const auto height{ static_cast<float>(canvas.get_height()) };

    const sve::vertices&& v{ .vertices{ { .x = 0.f, .y = 0.f, .r = 1.f, .g = 0.f, .b = 0.f },
                                        { .x = width - 1, .y = height - 1, .r = 0.f, .g = 1.f, .b = 0.f },
                                        { .x = width - 1, .y = 0, .r = 0.f, .g = 0.f, .b = 1.f },
                                        { .x = 0, .y = height - 1, .r = 1.f, .g = 1.f, .b = 1.f } } };

    sve::triangle_interpolated_render{ canvas }.draw_triangle_interpolated(v[0], v[1], v[2]);
    sve::triangle_interpolated_render{ canvas }.draw_triangle_interpolated(v[3], v[0], v[1]);
    sve::ppm_io::save_image("./results/interpolated_triangle.ppm", canvas.to_image());
}

auto do_interpolated_triangles_render() -> void
{
    namespace sve = soul_vessel_engine;

    sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

    const auto width{ static_cast<float>(canvas.get_width()) };
    const auto half_width{ width / 2 };
    const auto height{ static_cast<float>(canvas.get_height()) };
    const auto half_height{ height / 2 };

    const sve::vertices&& v{ .vertices{ { .x = half_width, .y = half_height, .r = 1.f, .g = 1.f, .b = 1.f },
                                        { .x = 0.f, .y = 0.f, .r = 1.f, .g = 0.f, .b = 0.f },
                                        { .x = half_width, .y = 0.f, .r = 1.f, .g = 0.5f, .b = 1.f },
                                        { .x = width - 1, .y = 0.f, .r = 1.f, .g = 0.6f, .b = 0.f },
                                        { .x = width - 1, .y = half_height, .r = 1.f, .g = 1.f, .b = 0.f },
                                        { .x = width - 1, .y = height - 1, .r = 0.f, .g = 1.f, .b = 0.f },
                                        { .x = half_width, .y = height - 1, .r = 0.2f, .g = 0.6f, .b = .9f },
                                        { .x = 0.f, .y = height - 1, .r = 0.f, .g = 0.f, .b = 1.f },
                                        { .x = 0.f, .y = half_height, .r = 0.6f, .g = 0.f, .b = 1.f } } };

    const sve::indices i{ 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 6, 0, 7, 6, 0, 7, 8, 0, 8, 1 };

    sve::triangles_interpolated_render{ canvas }.draw_triangles_interpolated(v, i);
    sve::ppm_io::save_image("./results/interpolated_triangles.ppm", canvas.to_image());
}

auto do_triangle_fun() -> void
{
    namespace sve = soul_vessel_engine;

    sve::canvas canvas{ sve::config::canvas::width, sve::config::canvas::height };

    const auto width{ static_cast<float>(canvas.get_width()) };
    const auto half_width{ width / 2 };
    const auto height{ static_cast<float>(canvas.get_height()) };
    const auto half_height{ height / 2 };

    const sve::vertices&& v{ .vertices{ { .x = half_width, .y = half_height, .r = 1.f, .g = 1.f, .b = 1.f },
                                        { .x = 0.f, .y = 0.f, .r = 1.f, .g = 0.f, .b = 0.f },
                                        { .x = half_width, .y = 0.f, .r = 1.f, .g = 0.5f, .b = 1.f },
                                        { .x = width - 1, .y = 0.f, .r = 1.f, .g = 0.6f, .b = 0.f },
                                        { .x = width - 1, .y = half_height, .r = 1.f, .g = 1.f, .b = 0.f },
                                        { .x = width - 1, .y = height - 1, .r = 0.f, .g = 1.f, .b = 0.f },
                                        { .x = half_width, .y = height - 1, .r = 0.2f, .g = 0.6f, .b = .9f },
                                        { .x = 0.f, .y = height - 1, .r = 0.f, .g = 0.f, .b = 1.f },
                                        { .x = 0.f, .y = half_height, .r = 0.6f, .g = 0.f, .b = 1.f } } };

    const sve::indices triangles_indexes{ 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 6, 0, 7, 6, 0, 7, 8, 0, 8, 1 };
    const sve::indices lines_indexes{ 0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8 };

    sve::triangles_interpolated_render{ canvas }.draw_triangles_interpolated(v, triangles_indexes);
    sve::indexed_render{ canvas }.draw_indexed(
        v, triangles_indexes, sve::primitive_type::line, sve::default_colors::black);
    sve::ppm_io::save_image("./results/triangles_fun.ppm", canvas.to_image());
}